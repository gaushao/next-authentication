import NextAuth from 'next-auth'
import Providers from 'next-auth/providers'

const {
  EMAIL_SERV,
  EMAIL_PORT,
  EMAIL_HOST,
  EMAIL_USERNAME,
  EMAIL_PASSWORD,
  EMAIL_FROM,
  GOOGLE_ID,
  GOOGLE_SECRET,
  DATABASE_URL,
} = process.env

export default NextAuth({
  // Configure one or more authentication providers
  providers: [
    Providers.Email({
      server: {
        service: EMAIL_SERV,
        port: EMAIL_SERV ? undefined : EMAIL_PORT,
        host: EMAIL_SERV ? undefined : EMAIL_HOST,
        secure: true,
        auth: {
          user: EMAIL_USERNAME,
          pass: EMAIL_PASSWORD,
        },
        // tls: {
        //   rejectUnauthorized: false,
        //   ciphers:'SSLv3',
        // },
      },
      from: EMAIL_FROM,
    }),
    Providers.Google({
      clientId: GOOGLE_ID,
      clientSecret: GOOGLE_SECRET,
    }),
    // ...add more providers here
  ],
  // A database is optional, but required to persist accounts in a database
  database: DATABASE_URL,
})
