# Authenticator
Application uses NextAuth.js to handle user authentication

## Install
```
yarn
```
optional: [MongoDB](https://www.mongodb.com/)

## Configure
- create a google account or use your own
- activate SMTP enabling `IMAP`
  [Google Settings](https://mail.google.com/mail/u/0/#settings/fwdandpop)
  [Google Support](https://support.google.com/mail/answer/7126229)
- go to [Google Cloud Console](https://console.cloud.google.com/)
- open new project and name it `nextauth` and select it
- go to [Google Cloud Credentials](https://console.cloud.google.com/apis/credentials)
- click on the `CONFIGURE CONSENT SCREEN` button
- check the `External` checkbox, then `CREATE`, `ADVANCE`, `SAVE`
- back to [Google Cloud Credentials](https://console.cloud.google.com/apis/credentials)
- click on the `CREATE CREDENTIALS` button, and choose `OAuth client ID`
- select `Web Application`
- fill the form
  javascript origins: http://localhost:3000
  redirect URIs: http://localhost:3000/api/auth/callback/google
- click `CREATE`
- copy `id` and `secret`
- create file at root folder called `.env.local`
- paste `id` and `secret` into `.env.local` as `GOOGLE_ID` and `GOOGLE_SECRET`
- paste the rest of the following variables into `.env.local`

```
NEXTAUTH_URL=http://localhost:3000
DATABASE_URL=mongodb://localhost:27017/nextauth

EMAIL_SERV=gmail
EMAIL_HOST=smtp.gmail.com
EMAIL_PORT=587

EMAIL_FROM=Your Name <youremail@gmail.com>
EMAIL_USERNAME=youremail@gmail.com
EMAIL_PASSWORD=yourpassword

GOOGLE_ID=PASTE_GOOGLE_ID
GOOGLE_SECRET=PASTE_GOOGLE_SECRET
```

## See More

[NextAuth.js](https://next-auth.js.org/)
[Google Sign-In](https://developers.google.com/identity/protocols/oauth2)

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.js`. The page auto-updates as you edit the file.

[API routes](https://nextjs.org/docs/api-routes/introduction) can be accessed on [http://localhost:3000/api/hello](http://localhost:3000/api/hello). This endpoint can be edited in `pages/api/hello.js`.

The `pages/api` directory is mapped to `/api/*`. Files in this directory are treated as [API routes](https://nextjs.org/docs/api-routes/introduction) instead of React pages.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## Deploy on Vercel

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.
